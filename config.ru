require 'rubygems'
require 'bundler'

Bundler.require

Dir[File.dirname(__FILE__) + '/lib/*.rb'].each { |file| require file }
require_relative 'robocar'

run Robocar
