getUrlParameter = (sParam) ->
  sPageURL = window.location.search.substring(1)
  sURLVariables = sPageURL.split('&')
  i = 0
  while i < sURLVariables.length
    sParameterName = sURLVariables[i].split('=')
    if sParameterName[0] == sParam
      return sParameterName[1]
    i++
fixEscape = (str) ->
  escape(str).replace('+', '%2B')
utm_source = getUrlParameter('utm_source')
utm_term = getUrlParameter('utm_term')
utm_medium = getUrlParameter('utm_medium')
utm_content = getUrlParameter('utm_content')
utm_campaign = getUrlParameter('utm_campaign')
$('.gallery').on 'click', 'a', (event) ->
  target = event.target
  link = if target.src then target.parentNode else target
  options =
    index: link
    event: event
  parent = $(@).closest('.gallery')
  links = $('a', parent)
  blueimp.Gallery links, options
$('a[href*=#]').on 'click', (event) ->
  event.preventDefault()
  $('html,body').animate { scrollTop: $(@hash).offset().top - 50 }, 1000
textValidation = (obj, state) ->
  if state
    obj.parent().removeClass('has-error')
    obj.next().removeClass('glyphicon-remove')
    obj.parent().addClass('has-success')
    obj.next().addClass('glyphicon-ok')
  else
    obj.next().removeClass('glyphicon-ok')
    obj.parent().removeClass('has-success')
    obj.parent().addClass('has-error')
    obj.next().addClass('glyphicon-remove')
$('.name').focusout ->
  if $(@).val().match(/[А-Яа-яA-Za-z]{3}/)
    textValidation $(@), true
  else
    textValidation $(@), false
$('.tel').mask "+7 (999) 999-99-99", completed: ->
  textValidation $(@), true
$('form').submit (event) ->
  event.preventDefault()
  telStatus = true
  nameStatus = true
  tel = $(@).find('.tel')
  name = $(@).find('.name')
  address = $(@).find('.address')
  unless tel.val().match(/\+7 \(\d{3}\) \d{3}(-\d\d){2}/)
    telStatus = false
    textValidation(tel, false)
  unless name.val().match(/[А-Яа-яA-Za-z]{3}/)
    nameStatus = false
    textValidation(name, false)
  if nameStatus and telStatus
    $(@).find(':submit').prop('disabled', true)
    $.post('clients', "name=#{name.val()}&address=#{address.val()}&phone=#{fixEscape(tel.val())}&utm_source=#{utm_source}&utm_term=#{utm_term}&utm_medium=#{utm_medium}&utm_campaign=#{utm_campaign}&utm_content=#{utm_content}")
      .done ->
        #yaCounter29120320.reachGoal('order').error()
        $('#modal').modal('hide')
        $('html,body').animate { scrollTop: $('#top').offset().top - 50 }, 1000
        $('#top').before('<div class="alert alert-success alert-dismissable text-center" role="alert"><button type="button" class="close" data-dismiss = "alert">&times;</button>Спасибо, что оставили заявку. Наш оператор свяжется с Вами в ближайшее время.</div>')
