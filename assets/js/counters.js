<!-- Google Analytics -->
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55148074-10', 'auto');
  ga('send', 'pageview');
<!-- /Google Analytics -->

<!-- Rating@Mail.ru counter -->
var _tmr = _tmr || [];
_tmr.push({id: "2635269", type: "pageView", start: (new Date()).getTime()});
(function (d, w) {
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true;
  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window);
<!-- /Rating@Mail.ru counter -->

<!-- vk.com -->
(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=dlFsSmOR1IBaWveYo8/ZZlNqYJ8IvkbfOORdn6VKTghIRULRSM3WLY08EIy/gpGgenNyDqoqUa5Rw1SY1Lio8m1sEyemMiHI8EooSH0nguss5Jf6yFh0x4FicrZGWGdERIS64tPzxIIYeU7DRWB8zqG3PHFVLhAaCKRLv6CN/6w-';
<!-- /vk.com -->

var google_conversion_id = 962047574;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
