class Robocar < Roda
  BitrixUri = URI('https://spiv.bitrix24.ru/crm/configs/import/lead.php')
  use Rack::Static, urls: ['/content']
  plugin :render, :engine => 'slim'
  plugin :assets, :css => ['blueimp-gallery.min.css', 'styles.scss'], :js => ['maskedinput.js', 'blueimp-gallery.min.js', 'counters.js', 'scripts.coffee']
  compile_assets

  route do |r|
    r.assets
    r.get do
      view('root')
    end
    r.post 'clients' do
      params = prepare_params(r.params)
      bitrix_id = create_lead(params)
      #tg_message(bitrix_id, params)
      params['phone'] = params['phone'].tr(' ()-', '')
      Client.create(client_name: params['name'], client_phone: params['phone'][2..-1],
                   source: params['utm_source'], medium: params['utm_medium'],
                   term: params['utm_term'], content: params['utm_content'],
                   campaign: params['utm_campaign'])
      r.redirect('/')
    end

  end
  def prepare_params(hash)
    params = {}
    hash.each do |k, v|
      v = nil if v == 'undefined'
      params[k] = v
    end
    params
  end
  def create_lead(params)
    crm = {}
    crm[:LOGIN] = 'barchonkoff@gmail.com'
    crm[:PASSWORD] = 'Qwerty12'
    crm[:TITLE] = 'robocar-poli.org'
    crm[:ASSIGNED_BY_ID] = 5
    crm[:NAME] = params['name']
    crm[:PHONE_MOBILE] = params['phone']
    crm[:UF_CRM_1424594100] = params['utm_source']
    crm[:UF_CRM_1424594110] = params['utm_medium']
    crm[:UF_CRM_1424594132] = params['utm_term']
    crm[:UF_CRM_1424594157] = params['utm_content']
    crm[:UF_CRM_1424594189] = params['utm_campaign']
    crm[:ADDRESS] = params['address']
    BitrixUri.query = URI.encode_www_form(crm)
    res = Net::HTTP.get_response(BitrixUri)
    id = res.body.sub(/.*'ID':'(\d+)'.*/, '\1')
  end
  #def tg_message(id, params)
    #spawn('/home/igor/robocar/tgmsg robocar-poli.org "\'№ %s\n%s\n%s\'"' % [id, params['name'], params['phone']])
  #end
end
