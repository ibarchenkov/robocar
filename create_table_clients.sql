CREATE TABLE clients
(
  client_id INTEGER PRIMARY KEY,
  client_name varchar(63),
  client_phone varchar(10),
  source varchar(15),
  medium varchar(15),
  term varchar(63),
  content varchar(63),
  campaign varchar(127),
  timestamp DATETIME DEFAULT (DATETIME('now', 'localtime'))
);
